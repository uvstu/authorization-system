package com.authorize.system.service.impl;

import java.util.List;
import com.authorize.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.authorize.system.mapper.AOrderMapper;
import com.authorize.system.domain.AOrder;
import com.authorize.system.service.IAOrderService;

/**
 * 订单管理Service业务层处理
 *
 * @author zwb
 * @date 2024-01-30
 */
@Service
public class AOrderServiceImpl implements IAOrderService
{
    @Autowired
    private AOrderMapper aOrderMapper;

    /**
     * 查询订单管理
     *
     * @param id 订单管理主键
     * @return 订单管理
     */
    @Override
    public AOrder selectAOrderById(Long id)
    {
        return aOrderMapper.selectAOrderById(id);
    }

    /**
     * 查询订单管理列表
     *
     * @param aOrder 订单管理
     * @return 订单管理
     */
    @Override
    public List<AOrder> selectAOrderList(AOrder aOrder)
    {
        return aOrderMapper.selectAOrderList(aOrder);
    }

    /**
     * 新增订单管理
     *
     * @param aOrder 订单管理
     * @return 结果
     */
    @Override
    public int insertAOrder(AOrder aOrder)
    {
        aOrder.setCreateTime(DateUtils.getNowDate());
        return aOrderMapper.insertAOrder(aOrder);
    }

    /**
     * 修改订单管理
     *
     * @param aOrder 订单管理
     * @return 结果
     */
    @Override
    public int updateAOrder(AOrder aOrder)
    {
        aOrder.setUpdateTime(DateUtils.getNowDate());
        return aOrderMapper.updateAOrder(aOrder);
    }

    /**
     * 批量删除订单管理
     *
     * @param ids 需要删除的订单管理主键
     * @return 结果
     */
    @Override
    public int deleteAOrderByIds(Long[] ids)
    {
        return aOrderMapper.deleteAOrderByIds(ids);
    }

    /**
     * 删除订单管理信息
     *
     * @param id 订单管理主键
     * @return 结果
     */
    @Override
    public int deleteAOrderById(Long id)
    {
        return aOrderMapper.deleteAOrderById(id);
    }

    /**
     * 根据商户单号查询信息
     * @param outTradeNo
     * @return
     */
    @Override
    public AOrder selectAOrderByOutTradeNo(String outTradeNo) {
        return aOrderMapper.selectAOrderByOutTradeNo(outTradeNo);
    }
}
