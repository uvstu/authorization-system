package com.authorize.system.service.impl;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import com.authorize.common.core.domain.entity.SysUser;
import com.authorize.common.exception.TClientException;
import com.authorize.common.utils.DateUtils;
import com.authorize.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.authorize.system.mapper.AEntryMapper;
import com.authorize.system.domain.AEntry;
import com.authorize.system.service.IAEntryService;

/**
 * 项目管理Service业务层处理
 *
 * @author zwb
 * @date 2024-01-29
 */
@Service
public class AEntryServiceImpl implements IAEntryService
{
    @Autowired
    private AEntryMapper aEntryMapper;

    /**
     * 查询项目管理
     *
     * @param id 项目管理主键
     * @return 项目管理
     */
    @Override
    public AEntry selectAEntryById(Long id)
    {
        return aEntryMapper.selectAEntryById(id);
    }

    /**
     * 查询项目管理列表
     *
     * @param aEntry 项目管理
     * @return 项目管理
     */
    @Override
    public List<AEntry> selectAEntryList(AEntry aEntry)
    {
        return aEntryMapper.selectAEntryList(aEntry);
    }

    /**
     * 新增项目管理
     *
     * @param aEntry 项目管理
     * @return 结果
     */
    @Override
    public int insertAEntry(AEntry aEntry) throws NoSuchAlgorithmException {
        SysUser sysUser = SecurityUtils.getLoginUser().getUser();
        aEntry.setCreateBy(sysUser.getUserName());
        aEntry.setCreateTime(DateUtils.getNowDate());
        aEntry.setUpdateBy(sysUser.getUserName());
        aEntry.setUpdateTime(DateUtils.getNowDate());
        return aEntryMapper.insertAEntry(aEntry);
    }

    /**
     * 修改项目管理
     *
     * @param aEntry 项目管理
     * @return 结果
     */
    @Override
    public int updateAEntry(AEntry aEntry) throws TClientException {
        SysUser sysUser = SecurityUtils.getLoginUser().getUser();
        aEntry.setUpdateBy(sysUser.getUserName());
        aEntry.setUpdateTime(DateUtils.getNowDate());
        return aEntryMapper.updateAEntry(aEntry);
    }

    /**
     * 批量删除项目管理
     *
     * @param ids 需要删除的项目管理主键
     * @return 结果
     */
    @Override
    public int deleteAEntryByIds(Long[] ids)
    {
        return aEntryMapper.deleteAEntryByIds(ids);
    }

    /**
     * 删除项目管理信息
     *
     * @param id 项目管理主键
     * @return 结果
     */
    @Override
    public int deleteAEntryById(Long id)
    {
        return aEntryMapper.deleteAEntryById(id);
    }
}
