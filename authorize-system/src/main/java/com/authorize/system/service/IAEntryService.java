package com.authorize.system.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import com.authorize.common.exception.TClientException;
import com.authorize.system.domain.AEntry;

/**
 * 项目管理Service接口
 *
 * @author zwb
 * @date 2024-01-29
 */
public interface IAEntryService
{
    /**
     * 查询项目管理
     *
     * @param id 项目管理主键
     * @return 项目管理
     */
    public AEntry selectAEntryById(Long id);

    /**
     * 查询项目管理列表
     *
     * @param aEntry 项目管理
     * @return 项目管理集合
     */
    public List<AEntry> selectAEntryList(AEntry aEntry);

    /**
     * 新增项目管理
     *
     * @param aEntry 项目管理
     * @return 结果
     */
    public int insertAEntry(AEntry aEntry) throws NoSuchAlgorithmException;

    /**
     * 修改项目管理
     *
     * @param aEntry 项目管理
     * @return 结果
     */
    public int updateAEntry(AEntry aEntry) throws TClientException;

    /**
     * 批量删除项目管理
     *
     * @param ids 需要删除的项目管理主键集合
     * @return 结果
     */
    public int deleteAEntryByIds(Long[] ids);

    /**
     * 删除项目管理信息
     *
     * @param id 项目管理主键
     * @return 结果
     */
    public int deleteAEntryById(Long id);
}
