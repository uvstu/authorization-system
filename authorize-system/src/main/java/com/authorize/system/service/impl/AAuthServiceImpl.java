package com.authorize.system.service.impl;

import java.util.List;

import com.authorize.common.exception.TClientException;
import com.authorize.common.utils.DateUtils;
import com.authorize.system.domain.AEntry;
import com.authorize.system.mapper.AEntryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.authorize.system.mapper.AAuthMapper;
import com.authorize.system.domain.AAuth;
import com.authorize.system.service.IAAuthService;

/**
 * 授权管理Service业务层处理
 *
 * @author zwb
 * @date 2024-01-29
 */
@Service
public class AAuthServiceImpl implements IAAuthService
{
    @Autowired
    private AAuthMapper aAuthMapper;

    @Autowired
    private AEntryMapper aEntryMapper;

    /**
     * 查询授权管理
     *
     * @param id 授权管理主键
     * @return 授权管理
     */
    @Override
    public AAuth selectAAuthById(Long id)
    {
        return aAuthMapper.selectAAuthById(id);
    }

    /**
     * 查询授权管理列表
     *
     * @param aAuth 授权管理
     * @return 授权管理
     */
    @Override
    public List<AAuth> selectAAuthList(AAuth aAuth)
    {
        return aAuthMapper.selectAAuthList(aAuth);
    }

    /**
     * 新增授权管理
     *
     * @param aAuth 授权管理
     * @return 结果
     */
    @Override
    public int insertAAuth(AAuth aAuth) throws TClientException {
        aAuth.setCreateTime(DateUtils.getNowDate());
        aAuth.setUpdateTime(DateUtils.getNowDate());
        aAuth.setAuthNo(System.currentTimeMillis()+"");
        if(aAuth.getAuthType() == 1){
            if(aAuth.getAuthValue() == null){
                throw new TClientException("新增失败，请填写授权域名");
            }
        }else{
            throw new TClientException("新增失败，非法授权类型");
        }
        return aAuthMapper.insertAAuth(aAuth);
    }

    /**
     * 修改授权管理
     *
     * @param aAuth 授权管理
     * @return 结果
     */
    @Override
    public int updateAAuth(AAuth aAuth) throws TClientException {
        aAuth.setUpdateTime(DateUtils.getNowDate());
        if(aAuth.getAuthType() == 1){
            if(aAuth.getAuthValue() == null){
                throw new TClientException("编辑失败，请填写授权域名");
            }
        }else{
            throw new TClientException("新增失败，非法授权类型");
        }
        return aAuthMapper.updateAAuth(aAuth);
    }

    /**
     * 批量删除授权管理
     *
     * @param ids 需要删除的授权管理主键
     * @return 结果
     */
    @Override
    public int deleteAAuthByIds(Long[] ids)
    {
        return aAuthMapper.deleteAAuthByIds(ids);
    }

    /**
     * 删除授权管理信息
     *
     * @param id 授权管理主键
     * @return 结果
     */
    @Override
    public int deleteAAuthById(Long id)
    {
        return aAuthMapper.deleteAAuthById(id);
    }

    @Override
    public AAuth insertAAuthLock(AAuth aAuth) throws TClientException {
        aAuth.setCreateTime(DateUtils.getNowDate());
        aAuth.setUpdateTime(DateUtils.getNowDate());
        aAuth.setStatus(aAuth.getStatus());
        if(aAuth.getAuthType() == 1){
            if(aAuth.getAuthValue() == null){
                throw new TClientException("新增失败，请填写授权域名");
            }
        }else{
            throw new TClientException("新增失败，非法授权类型");
        }
        if(aAuthMapper.selectAAuthByAuthNo(aAuth.getAuthNo()) == null){
            aAuthMapper.insertAAuth(aAuth);
        }
        return aAuthMapper.selectAAuthByAuthNo(aAuth.getAuthNo());
    }

    /**
     * 更新授权到期数据
     * @return
     */
    @Override
    public void updateAAuthEndTime() {
        aAuthMapper.updateAAuthEndTime();
    }

    /**
     * 根据编号查询授权信息
     * @param authNo
     * @return
     */
    @Override
    public AAuth selectAAuthByAuthNo(String authNo) {
        return aAuthMapper.selectAAuthByAuthNo(authNo);
    }
}
