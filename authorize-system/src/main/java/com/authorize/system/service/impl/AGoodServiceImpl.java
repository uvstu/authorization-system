package com.authorize.system.service.impl;

import java.util.List;
import com.authorize.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.authorize.system.mapper.AGoodMapper;
import com.authorize.system.domain.AGood;
import com.authorize.system.service.IAGoodService;

/**
 * 商品管理Service业务层处理
 * 
 * @author zwb
 * @date 2024-01-30
 */
@Service
public class AGoodServiceImpl implements IAGoodService 
{
    @Autowired
    private AGoodMapper aGoodMapper;

    /**
     * 查询商品管理
     * 
     * @param id 商品管理主键
     * @return 商品管理
     */
    @Override
    public AGood selectAGoodById(Long id)
    {
        return aGoodMapper.selectAGoodById(id);
    }

    /**
     * 查询商品管理列表
     * 
     * @param aGood 商品管理
     * @return 商品管理
     */
    @Override
    public List<AGood> selectAGoodList(AGood aGood)
    {
        return aGoodMapper.selectAGoodList(aGood);
    }

    /**
     * 新增商品管理
     * 
     * @param aGood 商品管理
     * @return 结果
     */
    @Override
    public int insertAGood(AGood aGood)
    {
        aGood.setCreateTime(DateUtils.getNowDate());
        return aGoodMapper.insertAGood(aGood);
    }

    /**
     * 修改商品管理
     * 
     * @param aGood 商品管理
     * @return 结果
     */
    @Override
    public int updateAGood(AGood aGood)
    {
        aGood.setUpdateTime(DateUtils.getNowDate());
        return aGoodMapper.updateAGood(aGood);
    }

    /**
     * 批量删除商品管理
     * 
     * @param ids 需要删除的商品管理主键
     * @return 结果
     */
    @Override
    public int deleteAGoodByIds(Long[] ids)
    {
        return aGoodMapper.deleteAGoodByIds(ids);
    }

    /**
     * 删除商品管理信息
     * 
     * @param id 商品管理主键
     * @return 结果
     */
    @Override
    public int deleteAGoodById(Long id)
    {
        return aGoodMapper.deleteAGoodById(id);
    }
}
