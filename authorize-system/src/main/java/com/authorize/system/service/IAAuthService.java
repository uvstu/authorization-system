package com.authorize.system.service;

import java.util.List;

import com.authorize.common.exception.TClientException;
import com.authorize.system.domain.AAuth;

/**
 * 授权管理Service接口
 *
 * @author zwb
 * @date 2024-01-29
 */
public interface IAAuthService
{
    /**
     * 查询授权管理
     *
     * @param id 授权管理主键
     * @return 授权管理
     */
    public AAuth selectAAuthById(Long id);

    /**
     * 查询授权管理列表
     *
     * @param aAuth 授权管理
     * @return 授权管理集合
     */
    public List<AAuth> selectAAuthList(AAuth aAuth);

    /**
     * 新增授权管理
     *
     * @param aAuth 授权管理
     * @return 结果
     */
    public int insertAAuth(AAuth aAuth) throws TClientException;

    /**
     * 修改授权管理
     *
     * @param aAuth 授权管理
     * @return 结果
     */
    public int updateAAuth(AAuth aAuth) throws TClientException;

    /**
     * 批量删除授权管理
     *
     * @param ids 需要删除的授权管理主键集合
     * @return 结果
     */
    public int deleteAAuthByIds(Long[] ids);

    /**
     * 删除授权管理信息
     *
     * @param id 授权管理主键
     * @return 结果
     */
    public int deleteAAuthById(Long id);

    /**
     * 单线程业务处理
     * @param aAuth
     * @return
     */
    AAuth insertAAuthLock(AAuth aAuth) throws TClientException;

    /**
     * 更新授权到期数据
     * @return
     */
    void updateAAuthEndTime();

    /**
     * 根据编号查询授权信息
     * @return
     */
    AAuth selectAAuthByAuthNo(String authNo);
}
