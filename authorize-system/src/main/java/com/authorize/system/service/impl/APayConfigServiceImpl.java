package com.authorize.system.service.impl;

import java.util.List;

import com.authorize.common.core.domain.entity.SysUser;
import com.authorize.common.utils.DateUtils;
import com.authorize.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.authorize.system.mapper.APayConfigMapper;
import com.authorize.system.domain.APayConfig;
import com.authorize.system.service.IAPayConfigService;

/**
 * 支付配置Service业务层处理
 *
 * @author zwb
 * @date 2024-01-29
 */
@Service
public class APayConfigServiceImpl implements IAPayConfigService
{
    @Autowired
    private APayConfigMapper aPayConfigMapper;

    /**
     * 查询支付配置
     *
     * @param id 支付配置主键
     * @return 支付配置
     */
    @Override
    public APayConfig selectAPayConfigById(Long id)
    {
        return aPayConfigMapper.selectAPayConfigById(id);
    }

    /**
     * 查询支付配置列表
     *
     * @param aPayConfig 支付配置
     * @return 支付配置
     */
    @Override
    public List<APayConfig> selectAPayConfigList(APayConfig aPayConfig)
    {
        return aPayConfigMapper.selectAPayConfigList(aPayConfig);
    }

    /**
     * 新增支付配置
     *
     * @param aPayConfig 支付配置
     * @return 结果
     */
    @Override
    public int insertAPayConfig(APayConfig aPayConfig)
    {
        SysUser sysUser = SecurityUtils.getLoginUser().getUser();
        aPayConfig.setCreateBy(sysUser.getUserName());
        aPayConfig.setCreateTime(DateUtils.getNowDate());
        aPayConfig.setUpdateBy(sysUser.getUserName());
        aPayConfig.setUpdateTime(DateUtils.getNowDate());
        return aPayConfigMapper.insertAPayConfig(aPayConfig);
    }

    /**
     * 修改支付配置
     *
     * @param aPayConfig 支付配置
     * @return 结果
     */
    @Override
    public int updateAPayConfig(APayConfig aPayConfig)
    {
        SysUser sysUser = SecurityUtils.getLoginUser().getUser();
        aPayConfig.setUpdateBy(sysUser.getUserName());
        aPayConfig.setUpdateTime(DateUtils.getNowDate());
        return aPayConfigMapper.updateAPayConfig(aPayConfig);
    }

    /**
     * 批量删除支付配置
     *
     * @param ids 需要删除的支付配置主键
     * @return 结果
     */
    @Override
    public int deleteAPayConfigByIds(Long[] ids)
    {
        return aPayConfigMapper.deleteAPayConfigByIds(ids);
    }

    /**
     * 删除支付配置信息
     *
     * @param id 支付配置主键
     * @return 结果
     */
    @Override
    public int deleteAPayConfigById(Long id)
    {
        return aPayConfigMapper.deleteAPayConfigById(id);
    }
}
