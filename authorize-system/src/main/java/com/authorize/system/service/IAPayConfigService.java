package com.authorize.system.service;

import java.util.List;
import com.authorize.system.domain.APayConfig;

/**
 * 支付配置Service接口
 * 
 * @author zwb
 * @date 2024-01-29
 */
public interface IAPayConfigService 
{
    /**
     * 查询支付配置
     * 
     * @param id 支付配置主键
     * @return 支付配置
     */
    public APayConfig selectAPayConfigById(Long id);

    /**
     * 查询支付配置列表
     * 
     * @param aPayConfig 支付配置
     * @return 支付配置集合
     */
    public List<APayConfig> selectAPayConfigList(APayConfig aPayConfig);

    /**
     * 新增支付配置
     * 
     * @param aPayConfig 支付配置
     * @return 结果
     */
    public int insertAPayConfig(APayConfig aPayConfig);

    /**
     * 修改支付配置
     * 
     * @param aPayConfig 支付配置
     * @return 结果
     */
    public int updateAPayConfig(APayConfig aPayConfig);

    /**
     * 批量删除支付配置
     * 
     * @param ids 需要删除的支付配置主键集合
     * @return 结果
     */
    public int deleteAPayConfigByIds(Long[] ids);

    /**
     * 删除支付配置信息
     * 
     * @param id 支付配置主键
     * @return 结果
     */
    public int deleteAPayConfigById(Long id);
}
