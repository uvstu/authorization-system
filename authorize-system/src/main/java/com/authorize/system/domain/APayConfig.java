package com.authorize.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.authorize.common.annotation.Excel;
import com.authorize.common.core.domain.BaseEntity;

/**
 * 支付配置对象 a_pay_config
 * 
 * @author zwb
 * @date 2024-01-29
 */
public class APayConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 支付名称 */
    @Excel(name = "支付名称")
    private String name;

    /** 支付渠道 */
    @Excel(name = "支付渠道")
    private String paymentChannel;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String appId;

    /** 签名密钥 */
    @Excel(name = "签名密钥")
    private String sginKey;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPaymentChannel(String paymentChannel) 
    {
        this.paymentChannel = paymentChannel;
    }

    public String getPaymentChannel() 
    {
        return paymentChannel;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setSginKey(String sginKey) 
    {
        this.sginKey = sginKey;
    }

    public String getSginKey() 
    {
        return sginKey;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("paymentChannel", getPaymentChannel())
            .append("appId", getAppId())
            .append("sginKey", getSginKey())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
