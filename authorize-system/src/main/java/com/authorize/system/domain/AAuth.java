package com.authorize.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.authorize.common.annotation.Excel;
import com.authorize.common.core.domain.BaseEntity;

/**
 * 授权管理对象 a_auth
 *
 * @author zwb
 * @date 2024-01-31
 */
public class AAuth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 授权编号 */
    @Excel(name = "授权编号")
    private String authNo;

    /** 授权项目 */
    @Excel(name = "授权项目")
    private Long authEntryId;

    /** 授权类型 */
    @Excel(name = "授权类型")
    private Integer authType;

    /** 授权值 */
    @Excel(name = "授权值")
    private String authValue;

    /** 到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 授权状态 */
    @Excel(name = "授权状态")
    private Integer status;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setAuthNo(String authNo)
    {
        this.authNo = authNo;
    }

    public String getAuthNo()
    {
        return authNo;
    }
    public void setAuthEntryId(Long authEntryId)
    {
        this.authEntryId = authEntryId;
    }

    public Long getAuthEntryId()
    {
        return authEntryId;
    }
    public void setAuthType(Integer authType)
    {
        this.authType = authType;
    }

    public Integer getAuthType()
    {
        return authType;
    }
    public void setAuthValue(String authValue)
    {
        this.authValue = authValue;
    }

    public String getAuthValue()
    {
        return authValue;
    }
    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }

    public Date getEndTime()
    {
        return endTime;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("authNo", getAuthNo())
            .append("authEntryId", getAuthEntryId())
            .append("authType", getAuthType())
            .append("authValue", getAuthValue())
            .append("endTime", getEndTime())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}
