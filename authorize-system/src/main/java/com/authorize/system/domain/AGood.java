package com.authorize.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.authorize.common.annotation.Excel;
import com.authorize.common.core.domain.BaseEntity;

/**
 * 商品管理对象 a_good
 * 
 * @author zwb
 * @date 2024-01-30
 */
public class AGood extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 授权项目 */
    @Excel(name = "授权项目")
    private Long entryId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodName;

    /** 授权类型 */
    @Excel(name = "授权类型")
    private Integer authType;

    /** 有效时间(月) */
    @Excel(name = "有效时间(月)")
    private Long effectiveTime;

    /** 商品价格 */
    @Excel(name = "商品价格")
    private BigDecimal price;

    /** 商品状态 */
    @Excel(name = "商品状态")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEntryId(Long entryId) 
    {
        this.entryId = entryId;
    }

    public Long getEntryId() 
    {
        return entryId;
    }
    public void setGoodName(String goodName) 
    {
        this.goodName = goodName;
    }

    public String getGoodName() 
    {
        return goodName;
    }
    public void setAuthType(Integer authType) 
    {
        this.authType = authType;
    }

    public Integer getAuthType() 
    {
        return authType;
    }
    public void setEffectiveTime(Long effectiveTime) 
    {
        this.effectiveTime = effectiveTime;
    }

    public Long getEffectiveTime() 
    {
        return effectiveTime;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("entryId", getEntryId())
            .append("goodName", getGoodName())
            .append("authType", getAuthType())
            .append("effectiveTime", getEffectiveTime())
            .append("price", getPrice())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
