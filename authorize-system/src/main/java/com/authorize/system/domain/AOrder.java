package com.authorize.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.authorize.common.annotation.Excel;
import com.authorize.common.core.domain.BaseEntity;

/**
 * 订单管理对象 a_order
 *
 * @author zwb
 * @date 2024-01-31
 */
public class AOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 商户单号 */
    @Excel(name = "商户单号")
    private String outTradeNo;

    /** 关联通道 */
    @Excel(name = "关联通道")
    private Long payId;

    /** 关联商品 */
    @Excel(name = "关联商品")
    private Long goodId;

    /** 授权ID */
    @Excel(name = "授权ID")
    private Long authId;

    /** 授权编号 */
    @Excel(name = "授权编号")
    private String authNo;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodName;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private Integer payMethod;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private BigDecimal price;

    /** 退款金额 */
    @Excel(name = "退款金额")
    private BigDecimal refundMoney;

    /** 授权内容 */
    @Excel(name = "授权内容")
    private String authValue;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private Integer status;

    /** 交易时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "交易时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date transactionTime;

    /** 退款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "退款时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date refundTime;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOutTradeNo(String outTradeNo)
    {
        this.outTradeNo = outTradeNo;
    }

    public String getOutTradeNo()
    {
        return outTradeNo;
    }
    public void setPayId(Long payId)
    {
        this.payId = payId;
    }

    public Long getPayId()
    {
        return payId;
    }
    public void setGoodId(Long goodId)
    {
        this.goodId = goodId;
    }

    public Long getGoodId()
    {
        return goodId;
    }
    public void setAuthId(Long authId)
    {
        this.authId = authId;
    }

    public Long getAuthId()
    {
        return authId;
    }
    public void setAuthNo(String authNo)
    {
        this.authNo = authNo;
    }

    public String getAuthNo()
    {
        return authNo;
    }
    public void setGoodName(String goodName)
    {
        this.goodName = goodName;
    }

    public String getGoodName()
    {
        return goodName;
    }
    public void setPayMethod(Integer payMethod)
    {
        this.payMethod = payMethod;
    }

    public Integer getPayMethod()
    {
        return payMethod;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setRefundMoney(BigDecimal refundMoney)
    {
        this.refundMoney = refundMoney;
    }

    public BigDecimal getRefundMoney()
    {
        return refundMoney;
    }
    public void setAuthValue(String authValue)
    {
        this.authValue = authValue;
    }

    public String getAuthValue()
    {
        return authValue;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setTransactionTime(Date transactionTime)
    {
        this.transactionTime = transactionTime;
    }

    public Date getTransactionTime()
    {
        return transactionTime;
    }
    public void setRefundTime(Date refundTime)
    {
        this.refundTime = refundTime;
    }

    public Date getRefundTime()
    {
        return refundTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("outTradeNo", getOutTradeNo())
            .append("payId", getPayId())
            .append("goodId", getGoodId())
            .append("authId", getAuthId())
            .append("authNo", getAuthNo())
            .append("goodName", getGoodName())
            .append("payMethod", getPayMethod())
            .append("price", getPrice())
            .append("refundMoney", getRefundMoney())
            .append("authValue", getAuthValue())
            .append("status", getStatus())
            .append("transactionTime", getTransactionTime())
            .append("refundTime", getRefundTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .toString();
    }
}
