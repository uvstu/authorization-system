package com.authorize.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.authorize.common.exception.TClientException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.authorize.common.annotation.Log;
import com.authorize.common.core.controller.BaseController;
import com.authorize.common.core.domain.AjaxResult;
import com.authorize.common.enums.BusinessType;
import com.authorize.system.domain.APayConfig;
import com.authorize.system.service.IAPayConfigService;
import com.authorize.common.utils.poi.ExcelUtil;
import com.authorize.common.core.page.TableDataInfo;

/**
 * 支付配置Controller
 *
 * @author zwb
 * @date 2024-01-29
 */
@RestController
@RequestMapping("/system/payconfig")
public class APayConfigController extends BaseController
{
    @Autowired
    private IAPayConfigService aPayConfigService;

    /**
     * 查询支付配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:payconfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(APayConfig aPayConfig)
    {
        startPage();
        List<APayConfig> list = aPayConfigService.selectAPayConfigList(aPayConfig);
        return getDataTable(list);
    }

    /**
     * 导出支付配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:payconfig:export')")
    @Log(title = "支付配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, APayConfig aPayConfig)
    {
        List<APayConfig> list = aPayConfigService.selectAPayConfigList(aPayConfig);
        ExcelUtil<APayConfig> util = new ExcelUtil<APayConfig>(APayConfig.class);
        util.exportExcel(response, list, "支付配置数据");
    }

    /**
     * 获取支付配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:payconfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(aPayConfigService.selectAPayConfigById(id));
    }

    /**
     * 新增支付配置
     */
    @PreAuthorize("@ss.hasPermi('system:payconfig:add')")
    @Log(title = "支付配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody APayConfig aPayConfig) throws TClientException {
        return toAjax(aPayConfigService.insertAPayConfig(aPayConfig));
    }

    /**
     * 修改支付配置
     */
    @PreAuthorize("@ss.hasPermi('system:payconfig:edit')")
    @Log(title = "支付配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody APayConfig aPayConfig) throws TClientException {
        return toAjax(aPayConfigService.updateAPayConfig(aPayConfig));
    }

    /**
     * 删除支付配置
     */
    @PreAuthorize("@ss.hasPermi('system:payconfig:remove')")
    @Log(title = "支付配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(aPayConfigService.deleteAPayConfigByIds(ids));
    }
}
