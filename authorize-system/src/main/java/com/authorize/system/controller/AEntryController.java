package com.authorize.system.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.authorize.common.exception.TClientException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.authorize.common.annotation.Log;
import com.authorize.common.core.controller.BaseController;
import com.authorize.common.core.domain.AjaxResult;
import com.authorize.common.enums.BusinessType;
import com.authorize.system.domain.AEntry;
import com.authorize.system.service.IAEntryService;
import com.authorize.common.utils.poi.ExcelUtil;
import com.authorize.common.core.page.TableDataInfo;

/**
 * 项目管理Controller
 *
 * @author zwb
 * @date 2024-01-29
 */
@RestController
@RequestMapping("/system/entry")
public class AEntryController extends BaseController
{
    @Autowired
    private IAEntryService aEntryService;

    /**
     * 查询项目管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:entry:list')")
    @GetMapping("/list")
    public TableDataInfo list(AEntry aEntry)
    {
        startPage();
        List<AEntry> list = aEntryService.selectAEntryList(aEntry);
        return getDataTable(list);
    }

    /**
     * 导出项目管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:entry:export')")
    @Log(title = "项目管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AEntry aEntry)
    {
        List<AEntry> list = aEntryService.selectAEntryList(aEntry);
        ExcelUtil<AEntry> util = new ExcelUtil<AEntry>(AEntry.class);
        util.exportExcel(response, list, "项目管理数据");
    }

    /**
     * 获取项目管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:entry:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(aEntryService.selectAEntryById(id));
    }

    /**
     * 新增项目管理
     */
    @PreAuthorize("@ss.hasPermi('system:entry:add')")
    @Log(title = "项目管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AEntry aEntry) throws NoSuchAlgorithmException, TClientException {
        return toAjax(aEntryService.insertAEntry(aEntry));
    }

    /**
     * 修改项目管理
     */
    @PreAuthorize("@ss.hasPermi('system:entry:edit')")
    @Log(title = "项目管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AEntry aEntry) throws TClientException {
        return toAjax(aEntryService.updateAEntry(aEntry));
    }

    /**
     * 删除项目管理
     */
    @PreAuthorize("@ss.hasPermi('system:entry:remove')")
    @Log(title = "项目管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(aEntryService.deleteAEntryByIds(ids));
    }
}
