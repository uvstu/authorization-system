package com.authorize.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.authorize.common.exception.TClientException;
import com.authorize.system.domain.AEntry;
import com.authorize.system.service.IAEntryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.authorize.common.annotation.Log;
import com.authorize.common.core.controller.BaseController;
import com.authorize.common.core.domain.AjaxResult;
import com.authorize.common.enums.BusinessType;
import com.authorize.system.domain.AGood;
import com.authorize.system.service.IAGoodService;
import com.authorize.common.utils.poi.ExcelUtil;
import com.authorize.common.core.page.TableDataInfo;

/**
 * 商品管理Controller
 *
 * @author zwb
 * @date 2024-01-30
 */
@RestController
@RequestMapping("/system/good")
public class AGoodController extends BaseController
{
    @Autowired
    private IAGoodService aGoodService;

    @Autowired
    private IAEntryService aEntryService;

    /**
     * 查询商品管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:good:list')")
    @GetMapping("/list")
    public TableDataInfo list(AGood aGood)
    {
        startPage();
        List<AGood> list = aGoodService.selectAGoodList(aGood);
        return getDataTable(list);
    }

    /**
     * 导出商品管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:good:export')")
    @Log(title = "商品管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AGood aGood)
    {
        List<AGood> list = aGoodService.selectAGoodList(aGood);
        ExcelUtil<AGood> util = new ExcelUtil<AGood>(AGood.class);
        util.exportExcel(response, list, "商品管理数据");
    }

    /**
     * 获取商品管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:good:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(aGoodService.selectAGoodById(id));
    }

    /**
     * 新增商品管理
     */
    @PreAuthorize("@ss.hasPermi('system:good:add')")
    @Log(title = "商品管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AGood aGood) throws TClientException {
        return toAjax(aGoodService.insertAGood(aGood));
    }

    /**
     * 修改商品管理
     */
    @PreAuthorize("@ss.hasPermi('system:good:edit')")
    @Log(title = "商品管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AGood aGood) throws TClientException {
        return toAjax(aGoodService.updateAGood(aGood));
    }

    /**
     * 删除商品管理
     */
    @PreAuthorize("@ss.hasPermi('system:good:remove')")
    @Log(title = "商品管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(aGoodService.deleteAGoodByIds(ids));
    }

    /**
     * 获取启用项目列表
     */
    @PreAuthorize("@ss.hasPermi('system:good:list')")
    @PostMapping("/entryList")
    public TableDataInfo entryList(){
        AEntry aEntry = new AEntry();
        List<AEntry> list = aEntryService.selectAEntryList(aEntry);
        return getDataTable(list);
    }
}
