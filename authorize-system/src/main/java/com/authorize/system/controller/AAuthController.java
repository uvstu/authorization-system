package com.authorize.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.authorize.common.exception.TClientException;
import com.authorize.system.domain.AEntry;
import com.authorize.system.service.IAEntryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.authorize.common.annotation.Log;
import com.authorize.common.core.controller.BaseController;
import com.authorize.common.core.domain.AjaxResult;
import com.authorize.common.enums.BusinessType;
import com.authorize.system.domain.AAuth;
import com.authorize.system.service.IAAuthService;
import com.authorize.common.utils.poi.ExcelUtil;
import com.authorize.common.core.page.TableDataInfo;

/**
 * 授权管理Controller
 *
 * @author zwb
 * @date 2024-01-29
 */
@RestController
@RequestMapping("/system/auth")
public class AAuthController extends BaseController
{
    @Autowired
    private IAAuthService aAuthService;

    @Autowired
    private IAEntryService aEntryService;

    /**
     * 查询授权管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:auth:list')")
    @GetMapping("/list")
    public TableDataInfo list(AAuth aAuth)
    {
        startPage();
        List<AAuth> list = aAuthService.selectAAuthList(aAuth);
        return getDataTable(list);
    }

    /**
     * 导出授权管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:auth:export')")
    @Log(title = "授权管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AAuth aAuth)
    {
        List<AAuth> list = aAuthService.selectAAuthList(aAuth);
        ExcelUtil<AAuth> util = new ExcelUtil<AAuth>(AAuth.class);
        util.exportExcel(response, list, "授权管理数据");
    }

    /**
     * 获取授权管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:auth:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(aAuthService.selectAAuthById(id));
    }

    /**
     * 新增授权管理
     */
    @PreAuthorize("@ss.hasPermi('system:auth:add')")
    @Log(title = "授权管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AAuth aAuth) throws TClientException {
        return toAjax(aAuthService.insertAAuth(aAuth));
    }

    /**
     * 修改授权管理
     */
    @PreAuthorize("@ss.hasPermi('system:auth:edit')")
    @Log(title = "授权管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AAuth aAuth) throws TClientException {
        return toAjax(aAuthService.updateAAuth(aAuth));
    }

    /**
     * 删除授权管理
     */
    @PreAuthorize("@ss.hasPermi('system:auth:remove')")
    @Log(title = "授权管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(aAuthService.deleteAAuthByIds(ids));
    }

    /**
     * 获取启用项目列表
     */
    @PreAuthorize("@ss.hasPermi('system:auth:list')")
    @PostMapping("/entryList")
    public TableDataInfo entryList(){
        AEntry aEntry = new AEntry();
        List<AEntry> list = aEntryService.selectAEntryList(aEntry);
        return getDataTable(list);
    }

}
