package com.authorize.system.mapper;

import java.util.List;
import com.authorize.system.domain.AGood;

/**
 * 商品管理Mapper接口
 * 
 * @author zwb
 * @date 2024-01-30
 */
public interface AGoodMapper 
{
    /**
     * 查询商品管理
     * 
     * @param id 商品管理主键
     * @return 商品管理
     */
    public AGood selectAGoodById(Long id);

    /**
     * 查询商品管理列表
     * 
     * @param aGood 商品管理
     * @return 商品管理集合
     */
    public List<AGood> selectAGoodList(AGood aGood);

    /**
     * 新增商品管理
     * 
     * @param aGood 商品管理
     * @return 结果
     */
    public int insertAGood(AGood aGood);

    /**
     * 修改商品管理
     * 
     * @param aGood 商品管理
     * @return 结果
     */
    public int updateAGood(AGood aGood);

    /**
     * 删除商品管理
     * 
     * @param id 商品管理主键
     * @return 结果
     */
    public int deleteAGoodById(Long id);

    /**
     * 批量删除商品管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAGoodByIds(Long[] ids);
}
