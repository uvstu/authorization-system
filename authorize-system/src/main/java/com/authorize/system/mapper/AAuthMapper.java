package com.authorize.system.mapper;

import java.util.List;
import com.authorize.system.domain.AAuth;

/**
 * 授权管理Mapper接口
 *
 * @author zwb
 * @date 2024-01-29
 */
public interface AAuthMapper
{
    /**
     * 查询授权管理
     *
     * @param id 授权管理主键
     * @return 授权管理
     */
    public AAuth selectAAuthById(Long id);

    /**
     * 查询授权管理列表
     *
     * @param aAuth 授权管理
     * @return 授权管理集合
     */
    public List<AAuth> selectAAuthList(AAuth aAuth);

    /**
     * 新增授权管理
     *
     * @param aAuth 授权管理
     * @return 结果
     */
    public int insertAAuth(AAuth aAuth);

    /**
     * 修改授权管理
     *
     * @param aAuth 授权管理
     * @return 结果
     */
    public int updateAAuth(AAuth aAuth);

    /**
     * 删除授权管理
     *
     * @param id 授权管理主键
     * @return 结果
     */
    public int deleteAAuthById(Long id);

    /**
     * 批量删除授权管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAAuthByIds(Long[] ids);

    /**
     * 根据授权编号查询数据
     * @param authNo
     * @return
     */
    AAuth selectAAuthByAuthNo(String authNo);

    /**
     * 更新授权到期数据
     * @return
     */
    public int updateAAuthEndTime();
}
