package com.authorize.system.mapper;

import java.util.List;
import com.authorize.system.domain.AOrder;

/**
 * 订单管理Mapper接口
 *
 * @author zwb
 * @date 2024-01-30
 */
public interface AOrderMapper
{
    /**
     * 查询订单管理
     *
     * @param id 订单管理主键
     * @return 订单管理
     */
    public AOrder selectAOrderById(Long id);

    /**
     * 查询订单管理列表
     *
     * @param aOrder 订单管理
     * @return 订单管理集合
     */
    public List<AOrder> selectAOrderList(AOrder aOrder);

    /**
     * 新增订单管理
     *
     * @param aOrder 订单管理
     * @return 结果
     */
    public int insertAOrder(AOrder aOrder);

    /**
     * 修改订单管理
     *
     * @param aOrder 订单管理
     * @return 结果
     */
    public int updateAOrder(AOrder aOrder);

    /**
     * 删除订单管理
     *
     * @param id 订单管理主键
     * @return 结果
     */
    public int deleteAOrderById(Long id);

    /**
     * 批量删除订单管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAOrderByIds(Long[] ids);

    /**
     * 根据商户单号查询信息
     * @param outTradeNo
     * @return
     */
    AOrder selectAOrderByOutTradeNo(String outTradeNo);
}
