package com.authorize.system.mapper;

import java.util.List;
import com.authorize.system.domain.AEntry;

/**
 * 项目管理Mapper接口
 * 
 * @author zwb
 * @date 2024-01-29
 */
public interface AEntryMapper 
{
    /**
     * 查询项目管理
     * 
     * @param id 项目管理主键
     * @return 项目管理
     */
    public AEntry selectAEntryById(Long id);

    /**
     * 查询项目管理列表
     * 
     * @param aEntry 项目管理
     * @return 项目管理集合
     */
    public List<AEntry> selectAEntryList(AEntry aEntry);

    /**
     * 新增项目管理
     * 
     * @param aEntry 项目管理
     * @return 结果
     */
    public int insertAEntry(AEntry aEntry);

    /**
     * 修改项目管理
     * 
     * @param aEntry 项目管理
     * @return 结果
     */
    public int updateAEntry(AEntry aEntry);

    /**
     * 删除项目管理
     * 
     * @param id 项目管理主键
     * @return 结果
     */
    public int deleteAEntryById(Long id);

    /**
     * 批量删除项目管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAEntryByIds(Long[] ids);
}
