package com.authorize.system.mapper;

import java.util.List;
import com.authorize.system.domain.APayConfig;

/**
 * 支付配置Mapper接口
 * 
 * @author zwb
 * @date 2024-01-29
 */
public interface APayConfigMapper 
{
    /**
     * 查询支付配置
     * 
     * @param id 支付配置主键
     * @return 支付配置
     */
    public APayConfig selectAPayConfigById(Long id);

    /**
     * 查询支付配置列表
     * 
     * @param aPayConfig 支付配置
     * @return 支付配置集合
     */
    public List<APayConfig> selectAPayConfigList(APayConfig aPayConfig);

    /**
     * 新增支付配置
     * 
     * @param aPayConfig 支付配置
     * @return 结果
     */
    public int insertAPayConfig(APayConfig aPayConfig);

    /**
     * 修改支付配置
     * 
     * @param aPayConfig 支付配置
     * @return 结果
     */
    public int updateAPayConfig(APayConfig aPayConfig);

    /**
     * 删除支付配置
     * 
     * @param id 支付配置主键
     * @return 结果
     */
    public int deleteAPayConfigById(Long id);

    /**
     * 批量删除支付配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAPayConfigByIds(Long[] ids);
}
