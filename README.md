# 授权系统

#### 介绍
本系统主要用于项目授权，其他系统的授权可以直接在授权系统上进行管理，支持离线的RSA授权，支持域名在线授权，项目目的主要是为了帮助个人开发者实现系统授权功能，让开发者能更好的对自己的产品进行保护。

#### 软件架构

springboot+redis+mysql+nginx

#### 系统官网

[系统官网](http://www.uvstu.com/cms/xhFMWaio)

#### 功能介绍

1、RSA授权

使用RSA公私钥方式进行授权。

2、域名授权

对指定域名进行授权，其他项目通过接口进行校验当前域名是否授权。

3、项目版本管理

项目版本管理，对添加的项目可以进行项目版本管理，其他项目通过接口可以获取当前最新版本，然后进行下载更新。

#### 项目展示

![1](img/1.png)
![2](img/2.png)
![3](img/3.png)
![4](img/4.png)
![5](img/5.png)
![6](img/6.png)
![7](img/7.png)
![8](img/8.png)

#### 项目文档

[授权系统部署文档](http://authsys.uvstu.com/案例/授权系统部署文档.docx)

其他对接文档，部署完成项目后即可看到

#### 技术支持

[QQ交流群](https://qm.qq.com/cgi-bin/qm/qr?k=ueB6KNNVBMrukoeE551uOoLfqlvFtp3k&jump_from=webapi&authKey=DNxPKcl2IYCCnCSmb0upz8dDQC8jAvmYRGWQ817LjQCwYn2tGIlOO6XMnhXlmxVJ)

[联系作者](http://wpa.qq.com/msgrd?v=3&uin=1150549059&site=qq&menu=yes)

#### 个人博客

[个人博客](http://blog.ct35.top/blog/#/home)

#### 其他项目

♦ **[支付系统](https://gitee.com/uvstu/new-payment-system)** 

#### 说明

 **本平台已经申请软著，代码已经开源，商用请使用商业版进行授权后使用** 
 
**开源不易，好用请点赞支持，谢谢**



**微信扫描下方二维码，关注订阅号，可以免费激活商业版**

![qrcode](img/qrcode.jpg)

