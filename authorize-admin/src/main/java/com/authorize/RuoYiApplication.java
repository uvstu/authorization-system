package com.authorize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author ruoyi
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class RuoYiApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(RuoYiApplication.class, args);
        System.out.println("  作者：_时¤～光§\n  系统名称：授权系统\n  公司名称：福州优维计算机商行\n" +
                "  联系方式：QQ 1150549059 QQ群 391842995 \n  授权系统启动成功，欢迎使用！ \n" +
                "                                                                        \n" +
                "     ██               ██   ██                      ██               \n" +
                "    ████             ░██  ░██                     ░░                \n" +
                "   ██░░██   ██   ██ ██████░██       ██████  ██████ ██ ██████  █████ \n" +
                "  ██  ░░██ ░██  ░██░░░██░ ░██████  ██░░░░██░░██░░█░██░░░░██  ██░░░██\n" +
                " ██████████░██  ░██  ░██  ░██░░░██░██   ░██ ░██ ░ ░██   ██  ░███████\n" +
                "░██░░░░░░██░██  ░██  ░██  ░██  ░██░██   ░██ ░██   ░██  ██   ░██░░░░ \n" +
                "░██     ░██░░██████  ░░██ ░██  ░██░░██████ ░███   ░██ ██████░░██████\n" +
                "░░      ░░  ░░░░░░    ░░  ░░   ░░  ░░░░░░  ░░░    ░░ ░░░░░░  ░░░░░░ \n" +
                "                                                                     \n");
    }
}
