import request from '@/utils/request'

// 查询商品管理列表
export function listGood(query) {
  return request({
    url: '/system/good/list',
    method: 'get',
    params: query
  })
}

// 查询商品管理详细
export function getGood(id) {
  return request({
    url: '/system/good/' + id,
    method: 'get'
  })
}

// 新增商品管理
export function addGood(data) {
  return request({
    url: '/system/good',
    method: 'post',
    data: data
  })
}

// 修改商品管理
export function updateGood(data) {
  return request({
    url: '/system/good',
    method: 'put',
    data: data
  })
}

// 删除商品管理
export function delGood(id) {
  return request({
    url: '/system/good/' + id,
    method: 'delete'
  })
}

// 查询项目列表
export function entryList() {
  return request({
    url: '/system/good/entryList',
    method: 'post'
  })
}
