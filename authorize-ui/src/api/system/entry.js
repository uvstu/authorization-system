import request from '@/utils/request'

// 查询项目管理列表
export function listEntry(query) {
  return request({
    url: '/system/entry/list',
    method: 'get',
    params: query
  })
}

// 查询项目管理详细
export function getEntry(id) {
  return request({
    url: '/system/entry/' + id,
    method: 'get'
  })
}

// 新增项目管理
export function addEntry(data) {
  return request({
    url: '/system/entry',
    method: 'post',
    data: data
  })
}

// 修改项目管理
export function updateEntry(data) {
  return request({
    url: '/system/entry',
    method: 'put',
    data: data
  })
}

// 删除项目管理
export function delEntry(id) {
  return request({
    url: '/system/entry/' + id,
    method: 'delete'
  })
}
