import request from '@/utils/request'

// 查询支付配置列表
export function listPayconfig(query) {
  return request({
    url: '/system/payconfig/list',
    method: 'get',
    params: query
  })
}

// 查询支付配置详细
export function getPayconfig(id) {
  return request({
    url: '/system/payconfig/' + id,
    method: 'get'
  })
}

// 新增支付配置
export function addPayconfig(data) {
  return request({
    url: '/system/payconfig',
    method: 'post',
    data: data
  })
}

// 修改支付配置
export function updatePayconfig(data) {
  return request({
    url: '/system/payconfig',
    method: 'put',
    data: data
  })
}

// 删除支付配置
export function delPayconfig(id) {
  return request({
    url: '/system/payconfig/' + id,
    method: 'delete'
  })
}
