import request from '@/utils/request'

//查询项目列表
export function entryList() {
  return request({
    url: '/system/authCenter/entryList',
    method: 'post'
  })
}

//查询商品列表
export function goodList(query) {
  return request({
    url: '/system/authCenter/goodList',
    method: 'post',
    params: query
  })
}

//查询支付列表
export function payList() {
  return request({
    url: '/system/authCenter/payList',
    method: 'post'
  })
}

//查询商品列表
export function pay(data) {
  return request({
    url: '/system/authCenter/pay',
    method: 'post',
    params: data
  })
}

//查询我的订单
export function orderList(query) {
  return request({
    url: '/system/authCenter/orderList',
    method: 'get',
    params: query
  })
}

//查询我的授权
export function authList(query) {
  return request({
    url: '/system/authCenter/authList',
    method: 'get',
    params: query
  })
}

// 查询授权管理详细
export function getAuth(id) {
  return request({
    url: '/system/authCenter/getAuthInfo/' + id,
    method: 'get'
  })
}

