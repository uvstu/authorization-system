import request from '@/utils/request'

// 查询授权管理列表
export function listAuth(query) {
  return request({
    url: '/system/auth/list',
    method: 'get',
    params: query
  })
}

// 查询授权管理详细
export function getAuth(id) {
  return request({
    url: '/system/auth/' + id,
    method: 'get'
  })
}

// 新增授权管理
export function addAuth(data) {
  return request({
    url: '/system/auth',
    method: 'post',
    data: data
  })
}

// 修改授权管理
export function updateAuth(data) {
  return request({
    url: '/system/auth',
    method: 'put',
    data: data
  })
}

// 删除授权管理
export function delAuth(id) {
  return request({
    url: '/system/auth/' + id,
    method: 'delete'
  })
}

// 查询项目列表
export function entryList() {
  return request({
    url: '/system/auth/entryList',
    method: 'post'
  })
}
