package com.authorize.common.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Title: 返回统一格式化
 * @version V1.0
 * @author: zhengweibin
 * @Description:
 * @create: 2021-12-30 16:04
 */
public class Result<T> {

    /**
     * 返回码
     */
    private Integer code;

    /**
     * 请求返回信息描述
     */
    private String message;

    /**
     * 请求是否成功
     */
    private boolean isSucceed;

    /**
     * 返回数据
     */
    private T data;


    public Result() {
        code = 200;
        message = "操作成功";
        isSucceed = true;
    }

    public Result(Integer code, String message, boolean isSucceed) {
        this.code = code;
        this.message = message;
        this.isSucceed = isSucceed;
    }

    public void setError(String mag) {
        message = mag;
        isSucceed = false;
    }

    public void setSccess(String mag) {
        message = mag;
        isSucceed = true;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @JsonProperty(value = "isSucceed")
    public boolean isSucceed() {
        return isSucceed;
    }

    public void setSucceed(boolean succeed) {
        isSucceed = succeed;
    }
}
